import { Component, OnInit } from '@angular/core';
import { Department } from '../../../core/models/department';
import { ApiService } from '../../../core/services/api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public departmentsSub: Observable<Department[]>;
  public search: string;

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.departmentsSub = this.apiService.getDepartments();
  }

}

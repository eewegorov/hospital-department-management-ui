import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Department } from '../../../core/models/department';
import { ApiService } from '../../../core/services/api.service';
import { SubscriptionLike } from 'rxjs';

@Component({
  selector: 'app-department-card',
  templateUrl: './department-card.component.html',
  styleUrls: ['./department-card.component.scss']
})
export class DepartmentCardComponent implements OnInit, OnDestroy {
  @Input() public department: Department;
  private deleteDepartmentSub: SubscriptionLike;

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
  }

  public deleteDepartment(): void {
    this.deleteDepartmentSub = this.apiService.deleteDepartment(this.department.api_key).subscribe(
      (response: boolean) => {
        if (response) {
          alert('Removed!');
        }
      });
  }

  ngOnDestroy(): void {
    if (this.deleteDepartmentSub) {
      this.deleteDepartmentSub.unsubscribe();
    }
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentCardComponent } from './department-card.component';

describe('DepartmentCardComponent', () => {
  let component: DepartmentCardComponent;
  let fixture: ComponentFixture<DepartmentCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentCardComponent);
    component = fixture.componentInstance;
    component.department = {
      name: 'Oncology',
      api_key: '4047305dsf-4354-4355-dfsf534-34dsfdsffds',
      contact_person: {
        name: 'Frederika Sanchez',
        email: 'frederika.sanchez@gmail.com',
        telephone: '0897654321'
      }
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

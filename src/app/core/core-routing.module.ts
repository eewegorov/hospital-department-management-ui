import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '',
    loadChildren: () => import('../modules/home/home.module').then(m => m.HomeModule)
  },
  { path: 'create',
    loadChildren: () => import('../modules/edit/edit.module').then(m => m.EditModule)
  },
  { path: ':department_id',
    loadChildren: () => import('../modules/edit/edit.module').then(m => m.EditModule)
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }

import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Department } from '../models/department';
import { FakeBackendService } from './fake-backend.service';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private fakeBackendService: FakeBackendService) { }

  public getDepartments(): Observable<Department[]> {
    return this.fakeBackendService.departments.pipe(
      catchError(this.handleError)
    );
  }

  public deleteDepartment(departmentApiKey: string): Observable<boolean> {
    return this.fakeBackendService.deleteDepartment(departmentApiKey).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(): Observable<any> {
    return throwError('request error');
  }
}

import { Injectable } from '@angular/core';
import { Department } from '../models/department';
import { BehaviorSubject, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FakeBackendService {
  public departmentsArray: Department[];
  public departments: BehaviorSubject<Department[]> = new BehaviorSubject<Department[]>(null);

  constructor() {
    this.departmentsArray = [
      {
        name: 'Cardiology',
        api_key: '3457305dsf-3454-4345-duyif534-34dsfgfgfds',
        contact_person: {
          name: 'Evgeniy Egorov',
          email: 'eewegorov@gmail.com',
          telephone: '+79615011720'
        }
      },
      {
        name: 'Oncology',
        api_key: '4047305dsf-4354-4355-dfsf534-34dsfdsffds',
        contact_person: {
          name: 'Frederika Sanchez',
          email: 'frederika.sanchez@gmail.com',
          telephone: '0897654321'
        }
      }
    ];
    this.departments.next(this.departmentsArray);
  }

  public deleteDepartment(departmentApiKey: string): Observable<boolean> {
    this.departmentsArray = this.departmentsArray.filter(department => department.api_key !== departmentApiKey);
    this.departments.next(this.departmentsArray);
    return of(true);
  }
}

export interface Department {
  name: string;
  api_key: string;
  contact_person: ContactPerson;
}

export interface ContactPerson {
  name: string;
  email: string;
  telephone: string;
}

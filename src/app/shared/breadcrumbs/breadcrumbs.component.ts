import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {
  public section = 'Create';

  constructor(private router: Router) {
    this.router.events.subscribe((routerEvent: any) => {
      if (routerEvent.url) {
        const link = router.url.slice(1).split('/')[0] || null;
        this.section = link !== null ? link === 'create' ? 'Create' : 'Edit' : null;
      }
    });
  }

  ngOnInit(): void {
  }

}

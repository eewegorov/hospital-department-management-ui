import { Pipe, PipeTransform } from '@angular/core';
import { Department } from '../../core/models/department';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(departments: Department[], search: string = ''): Department[] {
    if (!search.trim()) {
      return departments;
    }
    return departments.filter(department => {
      return department.name.toLowerCase().includes(search.toLowerCase()) ||
        department.api_key.toLowerCase().includes(search.toLowerCase()) ||
        department.contact_person.name.toLowerCase().includes(search.toLowerCase()) ||
        department.contact_person.email.toLowerCase().includes(search.toLowerCase()) ||
        department.contact_person.telephone.toLowerCase().includes(search.toLowerCase());
    });
  }

}
